const button = document.querySelector('button');
const wrapper = document.querySelector('.wrapper');

button.addEventListener('click', async function() {
  let response1 = await fetch('https://api.ipify.org/?format=json');
  let geolocation = await response1.json();
  let response2 = await fetch(`http://ip-api.com/json/${geolocation.ip}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query`);
  let parameters = await response2.json();
  wrapper.innerHTML = `
  Континент: ${parameters.continent}<br>
  Країна: ${parameters.country}<br>
  Регіон: ${parameters.regionName}<br>
  Місто: ${parameters.city}<br>
  Район: ${parameters.district}<br>
  `
});



